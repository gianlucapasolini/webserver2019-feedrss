'''
    Elaborato Programmazione di Reti
            a.a. 2019/2020
           Pasolini Gianluca
           Matricola: 873645
              Traccia 2
 Web Server con feed rss e meteo dinamici
'''

#!/bin/env python
import sys, signal
import http.server
import socketserver
#new imports
import requests
import feedparser
import urllib.request
import os
import json
import threading 
import cgi
from bs4 import BeautifulSoup

#manage the wait witout busy waiting
waiting_refresh = threading.Event()

# primi articoli di ogni testata
first_articles = [] 

# Legge il numero della porta dalla riga di comando, e mette default 8080
if sys.argv[1:]:
  port = int(sys.argv[1])
else:
  port = 8080

# classe che mantiene le funzioni di SimpleHTTPRequestHandler e implementa
# il metodo get nel caso in cui si voglia fare un refresh
class ServerHandler(http.server.SimpleHTTPRequestHandler):        
    def do_GET(self):
        # Scrivo sul file AllRequestsGET le richieste dei client     
        with open("AllRequestsGET.txt", "a") as out:
          info = "GET request,\nPath: " + str(self.path) + "\nHeaders:\n" + str(self.headers) + "\n"
          out.write(str(info))
        if self.path == '/refresh':
            resfresh_contents()
            self.path = '/'
        http.server.SimpleHTTPRequestHandler.do_GET(self)
        
    def do_POST(self):
        try:
            # Salvo i vari dati inseriti
            form = cgi.FieldStorage(    
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST'})
            
            # Con getvalue prendo i dati inseriti dall'utente
            name = form.getvalue('name')
            email = form.getvalue('email')
            messaggio = form.getvalue('messaggio')

            # Stampo all'utente i dati che ha inviato
            output="Recensione inviata\n\n MESSAGGIO:\nNOME e COGNOME: " + name + "\nE-MAIL: " + email + "\nRECENSIONE: " + messaggio +"\n"
            self.send_response(200)
        except: 
            self.send_error(404, 'Bad request submitted.')
            return;
        
        self.end_headers()
        self.wfile.write(bytes(output, 'utf-8'))
        
        # Salvo in locale i vari messaggi in AllPOST
        with open("AllPOST.txt", "a") as out:
          info = "\n\nPOST request,\nNOME e COGNOME: " + name + "\nE-MAIL: " + email + "\nRECENSIONE: "+ messaggio +"\n"
          out.write(info)
        
# ThreadingTCPServer per gestire più richieste
server = socketserver.ThreadingTCPServer(('127.0.0.1',port), ServerHandler)

# la parte iniziale è identica per tutti i giornali
header_html = """
<html>
    <head>
        <style>
            h1 {
                text-align: center;
                margin: 0;
            }
            table {width:70%;}
            img {
                max-width:300;
                max-height:200px;
                width:auto;
            }
            td {width: 33%;}
            p {text-align:justify;}
            td {
                padding: 20px;
                text-align: center;
            }
            .topnav {
  		        overflow: hidden;
  		        background-color: #333;
  		    }
            .topnav a {
  		        float: left;
  		        color: #f2f2f2;
  		        text-align: center;
  		        padding: 14px 16px;
  		        text-decoration: none;
  		        font-size: 17px;
  		    }        
  		    .topnav a:hover {
  		        background-color: #ddd;
  		        color: black;
  		    }        
  		    .topnav a.active {
  		        background-color: #4CAF50;
  		        color: white;
  		    }
        </style>
    </head>
    <body>
        <title>Pasolini newspaper</title>
"""

# la barra di navigazione è identica per tutti i giornali
navigation_bar = """
        <br>
        <br>
        <br>
        <div class="topnav">
            <a class="active" href="http://127.0.0.1:{port}">Home</a>
  		    <a href="http://127.0.0.1:{port}/corriere_sera.html">Corriere della sera</a>
            <a href="http://127.0.0.1:{port}/il_sole_24ore.html">Il Sole 24 Ore</a>
            <a href="http://127.0.0.1:{port}/repubblica.html">Repubblica</a>
            <a href="http://127.0.0.1:{port}/internazionale.html">Internazionale</a>
            <a href="http://127.0.0.1:{port}/tomshw.html">Tom's Hardware</a>
            <a href="http://127.0.0.1:{port}/smartworld.html">SmartWorld</a>
  		    <a href="http://127.0.0.1:{port}/refresh" style="float: right">Aggiorna contenuti</a>
            <a href="http://127.0.0.1:{port}/info.pdf" download="info.pdf" style="float: right">Download info pdf</a>
            <a href="http://127.0.0.1:{port}/recensione.html" style="float: right">Recensione</a>
  		</div>
        <br><br>
        <table align="center">
""".format(port=port)

# la parte finale è identica per tutti i giornali
footer_html= """
        </table>
    </body>
</html>
"""

#la parte finale per la pagine principale dove è possibile fare una recensione
end_page_recensione = """
        <br><br>
		<form action="http://127.0.0.1:{port}/recensione" method="post" style="text-align: center;">
		  <h1><strong>SCRIVI UNA RECENSIONE</strong></h1><br>
		  <label for="name">Cognome Nome:</label><br>
		  <input type="text" id="name" name="name"><br>
		  <label for="email">Email:</label><br>
		  <input type="text" id="email" name="email"><br>
		  <label for="messaggio">Messaggio</label><br><textarea name="messaggio" id="messaggio" rows="2"></textarea><br><br>
		  <input type="submit" value="Invia">
		</form>
		<br>
    </body>
</html>
""".format(port=port)

def create_page_recensioni():
    f = open('recensione.html','w', encoding="utf-8")
    f.write(header_html + " <br><br><h1>Recensione</h1> " + navigation_bar + "</table>" + end_page_recensione)
    f.close()
  
# creo tutti i file utili per navigare.
def resfresh_contents():
    print("updating all contents")
    create_page_corriere_della_sera()
    create_page_sole_24ore()
    create_page_repubblica()
    create_page_internazionale()
    create_page_tomshw()
    create_page_SmartWorld()
    create_index_page()
    print("finished update")

# creazione della pagina specifica del sole 24 ore
# prendendo le informazioni direttamente dal feed rss
def create_page_sole_24ore():
    r = requests.get('https://www.ilsole24ore.com/rss/italia.xml')
    if (r.status_code == 200):
        if not os.path.exists('images/sole24Ore'):
            os.makedirs('images/sole24Ore')
        d = feedparser.parse(r.text)
        
        message = header_html + "<h1>Il Sole 24 Ore</h1>" + navigation_bar
        # gestione eccezzioni se nel feed rss sono meno di 6 informazioni
        try:
            for i in range(6):
                try:
                    url_images = "./images/sole24Ore/" + str(i) + ".jpg"
                    os.remove(url_images)
                except:
                    pass
                info_image = json.loads(str(d.entries[i].links[0]).replace("\'", "\""))
                urllib.request.urlretrieve(info_image['href'], url_images)
                # utilizzo metodo in comune per aggiungere le info nella tabella
                message = add_element_in_table(i, d.entries[i].link, url_images, d.entries[i].title, message)     
        except:
            pass
        message = message + footer_html
        f = open('il_sole_24ore.html','w', encoding="utf-8")
        f.write(message)
        f.close()
    else:
        print("Errore caricamento contenuti il sole 24 ore")

# creazione della pagina specifica di Repubblica
# prendendo le informazioni direttamente dal feed rss
def create_page_repubblica():
    r = requests.get('https://www.repubblica.it/rss/cronaca/rss2.0.xml')
    if (r.status_code == 200):
        if not os.path.exists('images/repubblica'):
            os.makedirs('images/repubblica')
        d = feedparser.parse(r.text)
        message = header_html + "<h1>Repubblica</h1>" + navigation_bar
        # gestione eccezzioni se nel feed rss sono meno di 6 informazioni
        try:
            for i in range(6):
                try:
                    url_images = "./images/repubblica/" + str(i) + ".jpg"
                    os.remove(url_images)
                except:
                    pass
                info_image = json.loads(str(d.entries[i].links[1]).replace("\'", "\""))
                urllib.request.urlretrieve(info_image['href'], url_images)
                # utilizzo metodo in comune per aggiungere le info nella tabella
                message = add_element_in_table(i, d.entries[i].link, url_images, d.entries[i].title, message)  
        except:
            pass
        message = message + footer_html
        f = open('repubblica.html','w', encoding="utf-8")
        f.write(message)
        f.close()
    else:
        print("Errore caricamento contenuti Repubblica")

# creazione della pagina specifica del corriere della sera
# prendendo le informazioni direttamente dal feed rss
def create_page_corriere_della_sera():
    create_page_img_html('http://xml2.corriereobjects.it/rss/homepage.xml', 'images/corrieresera', 'corriere_sera.html', 'Corrirere della Sera')
    
# creazione della pagina specifica dell' Internazionale
# prendendo le informazioni direttamente dal feed rss
def create_page_internazionale():
    create_page_img_html('https://www.internazionale.it/sitemaps/rss.xml', 'images/internazionale', 'internazionale.html', 'Internazionale')

# creazione della pagina specifica di Tom's Hardware
# prendendo le informazioni direttamente dal feed rss
def create_page_tomshw():
    create_page_img_html('https://www.tomshw.it/feed/', 'images/tomshw', 'tomshw.html', 'Tom\'s Hardware')

# creazione della pagina specifica di SmartWorld
# prendendo le informazioni direttamente dal feed rss
def create_page_SmartWorld():
    create_page_img_html('https://www.smartworld.it/feed', 'images/smartworld', 'smartworld.html', 'SmartWorld')

# metodo per eseguire l'aggiunga sulla tabella in comune per tutti
def add_element_in_table(i, link, url_images, title, message):
    if (i == 0):
        #il primo articolo di ogni testata va nella pagina di HOME
        first_articles.append('<td><a href="' + link + '"><img src="' + url_images + '"><br><p>'+ title + '</p></a></td>')
    if (i%3 == 0):
        message = message + "<tr>"
    message = message + '<td><a href="' + link + '"><img src="' + url_images + '"><br><p>'+ title + '</p></a></td>'
    if (i%3 == 2):
        message = message + "</tr>"
    return message

# metodo per create una pagina locale, trovando l'img dentro la descrizione
# che è in formato html perciò utilizzo beautifulsoup
# non funziona con tutte le pagine solo le 4 precedenti
def create_page_img_html(feed, image_url, name_page, title):
    r = requests.get(feed)
    if (r.status_code == 200):
        if not os.path.exists(image_url):
            os.makedirs(image_url)
        d = feedparser.parse(r.text)
        message = header_html + "<h1>" + title + "</h1>" + navigation_bar
        # gestione eccezzioni se nel feed rss sono meno di 6 informazioni
        try:
            for i in range(6):
                try:
                    url_images = "./" + image_url + "/" + str(i) + ".jpg"
                    os.remove(url_images)
                except:
                    pass
                soup = BeautifulSoup(d.entries[i].description, features="lxml")
                urllib.request.urlretrieve(soup.find('img')['src'], url_images)
                # utilizzo metodo in comune per aggiungere le info nella tabella
                message = add_element_in_table(i, d.entries[i].link, url_images, d.entries[i].title, message)   
        except:
            pass
        message = message + footer_html
        f = open(name_page,'w', encoding="utf-8")
        f.write(message)
        f.close()
    else:
        print("Errore caricamento contenuti " + title)
    
# creazione della pagina index.html (iniziale)
# contenente i primi articoli di ogni testata giornalistica
def create_index_page():
    f = open('index.html','w', encoding="utf-8")
    try:
        message = header_html + "<h1>Pasolini Newspaper</h1>" + navigation_bar
        message = message + '<tr><th colspan="2"><h2>Cronaca</h2></th>'
        message = message + '<th><h2>Tecnologia</h2></th></tr>'
        message = message + '<tr>' + first_articles[0] + first_articles[1]
        message = message + first_articles[4] + "</tr>"
        message = message + '<tr>' + first_articles[2] + first_articles[3]
        message = message + first_articles[5] + "</tr>"
        message = message + footer_html
    except:
        pass
    f.write(message)
    f.close()

# metodo per navigare facilmente dentro il file json del meteo      
def get_info_weather(json, key):
    return json['main'][key]

# ricavo dal file json il nome della icon da prendere sul sito.
def get_icon_data_meteo(json):
    icon_id = json['weather'][0]['icon']
    url = 'http://openweathermap.org/img/wn/{icon}.png'.format(icon=icon_id)
    return url

# metodo per ricavare tutte le informazioni utili per il meteo, con API
# come temperatura e immagine esplicativa
def get_meteo_img():
    while True:
        APPID = 'f93543a5d8e84d9ba1ba68ec3eb8ec40'
        url = "http://api.openweathermap.org/data/2.5/weather?appid={appid}&q={city},{state}&units=metric"
        url = url.format(appid=APPID, city="Rimini", state="it")
        json = requests.get(url).json()
        global header_html        
        header_html += '<h4 style="margin: 0;">Temperatura Rimini: @temp@</h4>'.replace("@temp@", str(get_info_weather(json,"temp")))
        header_html += '<img src="@url_icon_meteo@">'.replace("@url_icon_meteo@", str(get_icon_data_meteo(json)))
        #chiamo il metodo per generare i contenuti
        resfresh_contents()  
        if waiting_refresh.wait(timeout=300):
            break
    print("Stopping Thread refresh img")
   
# lancio un thread che inizialmente carina il meteo per la città di rimini
# questo thread ogni 300 secondi (5 minuti) aggiorna il meteo e i relativi
# contenuti delle pagine     
def launch_thread_resfresh():
    t_refresh = threading.Thread(target=get_meteo_img)
    t_refresh.daemon = True
    t_refresh.start()
    
# definiamo una funzione per permetterci di uscire dal processo tramite Ctrl-C
def signal_handler(signal, frame):
    print( 'Exiting http server (Ctrl+C pressed)')
    try:
      if(server):
        server.server_close()
    finally:
      # fermo il thread del refresh senza busy waiting
      waiting_refresh.set()
      sys.exit(0)
      
# metodo che viene chiamato al "lancio" del server
def main():
    # lancio un thread che carica il meteo e aggiorna ricorrentemente i contenuti
    launch_thread_resfresh()
    #Assicura che da tastiera usando la combinazione
    #di tasti Ctrl-C termini in modo pulito tutti i thread generati
    server.daemon_threads = True 
    #il Server acconsente al riutilizzo del socket anche se ancora non è stato
    #rilasciato quello precedente, andandolo a sovrascrivere
    server.allow_reuse_address = True  
    #interrompe l'esecuzione se da tastiera arriva la sequenza (CTRL + C) 
    signal.signal(signal.SIGINT, signal_handler)
    #creo pagina recensioni
    create_page_recensioni()
    # cancella i dati get ogni volta che il server viene attivato
    f = open('AllRequestsGET.txt','w', encoding="utf-8")
    f.close()
    # entra nel loop infinito
    try:
      while True:
        server.serve_forever()
    except KeyboardInterrupt:
      pass
    server.server_close()

if __name__ == "__main__":
    main()
