# Pasolini Newspaper

Elaborato Programmazione di reti.

## Traccia 2

Si immagini di dover realizzare un Web Server in Python per una testata giornalistica di cronaca, o
di sport o di moda (con relative directory e file accessori), che supporti più connessioni in
contemporanea.\
Il Web Server (da realizzare sul localhost) dovrà consentire la visualizzazione della pagina iniziale
(index.html) in cui siano presenti immagini e i titoli degli articoli. I titoli degli articoli principali
dovranno contenere dei link ad altre pagine web (interne al web server) in cui il testo dell’articolo
viene mostrato per esteso.\
Nella pagina principale dovrà anche essere presente un link per il download di un file pdf da parte
del client.

## Informazioni personali
Nome e cognome: Gianluca Pasolini \
Mail: gianluca.pasolini2@studio.unibo.it \
Numero matricola : 0000873645

## Specifiche progetto

Questo progetto ha l'obbiettivo di creare un web Server in Python, rendendo visibili in maniera chiara e intuitiva le news. Tutte le informazioni presenti su questo Web Server sono ricavate dai feed rss dinamici delle rispettive testate giornalistiche:
* Corriere della sera
* Il Sole 24 Ore
* Repubblica
* Internazionale
* Tom's Hardware
* SmartWorld

Inoltre in alto a sinistra è presente il meteo in tempo reale su Rimini con la corrispettiva immagine.

Oltre alle notizie è possibile effettuare 3 operazioni:
* Recensione
* Download pdf
* Aggiornamento informazioni

Cliccando su Recensione nella barra di navigazione sarai reindirizzato alla pagina recensione.html nella quale è presente un form da compilare. Tutte le recensioni vengono salvate nel file AllPOST.txt.

Cliccando su Download info pdf verrà scaricato in automatico un file generico pdf.

Cliccando su Aggiorna contenuti il main thread del server aggiorna i contenuti ricavando le nuove informazioni dai feed rss.

Infine tutte le richieste effettuate sul webserver vengono salvate nel file AllRequestsGET.txt per mantenere uno storico delle azioni eseguite. Questo file viene azzerato ogni volta che il webserver viene riavviato.

## Librerie esterne necessarie

Ho utilizzato diverse librerie esterne tra cui:
* requests
* feedparser
* bs4 (nello specifico BeautifulSoup)

Consiglio di utilizzare il package manager [pip](https://pip.pypa.io/en/stable/) per installare le sopracitate librerie se non sono presenti.

```bash
pip install requests
pip install feedparser
pip install bs4
pip install lxml
```

## Utilizzo

Dalla console è necessario avviare il server con il seguente comando. 

Il parametro PORT specifica la porta su cui aprire il web server, ma questa è opzionale (default 8080).

```bash
python PasoliniNewspaper_Server.py PORT
```

Una volta avviato il web server il main-thread genera un altro thread che aggiorna i contenuti ricavando le informazioni dai feed rss e rimane in riposo per 300 secondi, per poi aggiornarle nuovamente. Invece il main-thread rimane in sempre in ascolto per fornire ai client le varie informazioni richieste. \
Dunque una volta avviato è possibile aprire diversi browser che fungono da client facendo la richiesta al seguente link: [http://127.0.0.1:PORT](http://127.0.0.1:8080). Al suo interno si ha la possibilità di vedere la prima notizia di ogni testata giornalista e, attraverso la barra di navigazione, ci si può spostare per vedere le ultime 6 news di un giornale specifico. Alla destra della barra di navigazione sono presenti 3 funzioni extra:
* Download info pdf
* Aggiorna contenuti
* Recensione

Cliccando sul primo si ha la possibilità di scaricare il file locale "info.pdf". \
Mentre se si clicca sul secondo è possibile fare un aggiornamento di tutti i contenuti precedentemente scaricati. Questa manovra viene fatta anche in automatico da un Thread apposito ogni 5 minuti, per avere le informazioni giornalistiche e del meteo sempre aggiornate. Se controlliamo in alto a sinistra nella nostra finestra di navigazione vedremo la temperatura in tempo reale di Rimini e una immagine che rappresenta la relativa condizione atmosferica. Queste informazioni sono state ricavare attraverso richieste API direttamente a [OpenWeather](https://openweathermap.org/current). \
Infine cliccando su Recensione è possibile creare appunto una recensione del sito.

## License
[MIT](https://choosealicense.com/licenses/mit/)

```text
MIT License

Copyright (c) 2020 Gianluca Pasolini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```